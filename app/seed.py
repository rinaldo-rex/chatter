# Seeding base data for the chatbot to handle the converstational mailing
import click
import logging
import pandas as pd
import os

from flask import current_app as app
from flask.cli import with_appcontext
from mongoengine import connect
logger = logging.getLogger(__name__)


def seed_base_training_data(filename='greetings.csv'):
    path = os.path.join(os.curdir, app.config['TRAINING_DATA_DIR'], filename)
    # Using pandas because this can easily handle huge data in the future
    df = pd.read_csv(path)
    from app.models import BaseQuery, BaseResponse

    logger.debug('Seeding training data.')

    csv_header = df.columns.values.tolist()
    logger.debug('File header: {}'.format(str(csv_header)))
    for item in df.values:
        if str(filename).__contains__('query'):
            train_query = BaseQuery(
                text=str(item[1]),
                category=str(item[2]).upper(),
            )
            train_query.save()
            logger.debug('Seeded query: {}'.format(train_query.text))
        elif str(filename).__contains__('resp'):
            train_response = BaseResponse(
                text=str(item[1]),
                category=str(item[2]).upper(),
            )
            train_response.save()
            logger.debug('Seeded query: {}'.format(train_response.text))


def drop_collections():
    """
    This drops the database and all data will be lost. Be careful.

    :return:
    """
    db = connect(host=app.config['MONGODB_URI'])
    db.drop_database(app.config['MONGODB_NAME'])


@click.command('seed')
@with_appcontext
def seed():
    """
    This seed function is used to seed all the training data.

    :return:
    """
    drop_collections()
    path = os.path.join(os.curdir, app.config['TRAINING_DATA_DIR'])
    try:
        for file in os.listdir(path):
            seed_base_training_data(file)
    except FileNotFoundError as e:
        logger.error("Training data error: {}".format(e))
        logger.error("Training data doesn't exist at : {}".format(
            app.config['TRAINING_DATA_DIR']
        ))
