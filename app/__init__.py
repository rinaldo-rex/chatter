"""
Initialize the application here.

For advanced use cases, you can use application factory
"""

import logging
import os
import spacy

from flask import Flask
from logging.config import dictConfig

from .seed import seed

try:
    from app import config
except ImportError as e:
    print("Error: Unable to import config, setting values from env variables")

app = Flask(__name__)

try:
    app.config.from_object('app.config')
    app.secret_key = app.config['SECRET_KEY']
    app.template_folder = app.config.get('TEMPLATE_DIR', '../templates')
except ImportError as e:
    APP_NAME = 'chatter'
    APP_PORT = 3000
    LOG_DIR = 'logs'
    app.config['LOGGING'] = {
        'version': 1,
        'disable_existing_loggers': False,
        'root': {
            'level': logging.DEBUG,
            'handlers': ['console', 'file'],
        },
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'level': logging.DEBUG,
                'formatter': 'detailed',
                'stream': 'ext://sys.stdout',
            },
            'file': {
                'class': 'logging.handlers.RotatingFileHandler',
                'level': logging.DEBUG,
                'formatter': 'detailed',
                # 'filename': '' + APP_NAME.lower() + '.log',
                'filename': '{}/{}.log'.format(LOG_DIR, APP_NAME.lower()),
                'mode': 'a',
                'maxBytes': 10485760,
                'backupCount': 5,
                # 'filters': ['contextFilter'],
            }
        },
        'formatters': {
            'detailed': {
                'format': ('%(asctime)s %(name)-17s line:%(lineno)-4d '
                            '%(levelname)-8s %(message)s')
            },
            'with_filter': {
                'format': ('%(asctime)s %(name)-17s line:%(lineno)-4d '
                            '%(levelname)-8s User: %(user)-35s %(message)s')
            },
        },
    }
    app.config['SECRET_KEY'] = os.getenv('SECRET_KEY')
    app.config['MONGODB_URI'] = os.getenv('MONGODB_URI')
    app.config['MAILGUN_API_KEY'] = os.getenv('MAILGUN_API_KEY')
    app.config['MAILGUN_BASE_URL'] = os.getenv('MAILGUN_BASE_URL')
    app.config['MAILGUN_EMAIL_SUFFIX'] = os.getenv('MAILGUN_EMAIL_SUFFIX')
    app.config['TRAINING_DATA_DIR'] = os.getenv('TRAINING_DATA_DIR')
    app.config['MONGODB_NAME'] = os.getenv('MONGODB_NAME')
    app.config['DEFAULT_SUPPORT_MAIL'] = os.getenv('DEFAULT_SUPPORT_MAIL')




app.secret_key = app.config['SECRET_KEY']
app.template_folder = app.config.get('TEMPLATE_DIR', '../templates')


nlp = spacy.load('en_core_web_sm')  # Default English corpus, Note: Heavy.

# Importing here to avoid circular import error. :)
from .routes import *
from .models import db
from .utils import BotMailer

bot = BotMailer()
# Configure logger
dictConfig(app.config['LOGGING'])
logger = logging.getLogger(__name__)

app.cli.add_command(seed)  # For seeding the data