"""
The models include all the database model. The M in MVC/MTV pattern if you will.

Please note that for future usecases with many models, it's better to have
a directory of model files.
"""

from mongoengine import (
    Document,
    StringField,
)
from mongoengine import connect
from . import app
BASE_CATEGORIES = (
    'GREETING',
    'CONTACT_REQUEST',
)

BASE_TYPES = (
    'QUERY',
    'RESPONSE',
)

# Mongodb connection
db = connect(host=app.config['MONGODB_URI'])


class BaseQuery(Document):
    """
    The base query is any query raised by the user to the chatbot.

    It's better to have a separate collection from the responses because this
    will later help in better performance (because in an ideal system, the docs
    on Query and Response model will be really huge) and also in later analysis
    and enhancement of the chatbot. :)  This shall be used for the NLU part of
    the bot.
    """

    text = StringField(max_length=200)
    category = StringField(choices=BASE_CATEGORIES)
    type_ = StringField(choices=BASE_TYPES, default='QUERY')


class BaseResponse(Document):
    """
    The base response is any response that is to be given/provided to the user from
    the bot.

    The shall be used for the NLG part of the bot.
    """

    # Responses can be huge because it'll typically include more details altogether.
    text = StringField(max_length=500)
    category = StringField(choices=BASE_CATEGORIES)
    type_ = StringField(choices=BASE_TYPES, default='RESPONSE')
